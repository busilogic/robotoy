package main

import (
    "bufio"
    "os"
    "fmt"
    "strings"
    "strconv"
)

// Encapsulate Robot state
type Robot struct {
     currentDirection string
     currentX, currentY int
}

// Encapsulate Board state
type TableTop struct {

     availableDirection []string
     xMinBounds, xMaxBounds, yMinBounds, yMaxBounds int
}


/* Set x coordinate of robot if in bounds of table top */
func (robot *Robot) setX(targetX int, tableTop *TableTop){
     if targetX <= (*tableTop).xMaxBounds && targetX >= (*tableTop).xMinBounds {
        (*robot).currentX = targetX
     }     
}


/* Set y coordinate of robot if in bounds of table top */
func (robot *Robot) setY(targetY int, tableTop *TableTop){
     if targetY <= (*tableTop).yMaxBounds && targetY >= (*tableTop).yMinBounds {
        (*robot).currentY = targetY
     }     
}


/* Set y coordinate of robot if in bounds of table top */
func (robot *Robot) setDirection(targetDirection string, tableTop *TableTop){

    //Find matching string and return index
    for i := 0; i < len(tableTop.availableDirection); i++ {
       //fmt.Println("Value of i is now:", i, tableTop.availableDirection[i])
        
        if targetDirection == tableTop.availableDirection[i] {
            //fmt.Println("Bingo! found it", i)
            (*robot).currentDirection = tableTop.availableDirection[i]
        }

    }//for
    
}

/* Check if valid direction */
func directionIndex(directionStr string, tableTop TableTop) int{

     // Given a direction, return index if valid direction

     for i := 0; i < len(tableTop.availableDirection); i++ {
         //fmt.Println("Value of i is now:", i, tableTop.availableDirection[i])
        
         if directionStr == tableTop.availableDirection[i] {
            return i
         }
     }
    return -1
}

// Helper to get index of currentDirection in availableDirection String array
func currentDirectionIndex(robot Robot, tableTop TableTop) int {

    //Find matching string and return index
    for i := 0; i < len(tableTop.availableDirection); i++ {
       //fmt.Println("Value of i is now:", i, tableTop.availableDirection[i])
        
        if robot.currentDirection == tableTop.availableDirection[i] {
            //fmt.Println("Bingo! found it", i)
            return i
        }else{
            //fmt.Println("No Match")
        }

    }//for
    
    return -1    

}


// Output current location
func reportLocation(robot *Robot){
  fmt.Println("Output: ", robot.currentX, ",", robot.currentY, robot.currentDirection)
}


func turnLeft(robot *Robot, tableTop *TableTop){

     dirIndex := directionIndex((*robot).currentDirection, (*tableTop))
     if dirIndex != -1 {  // starting with valid direction

        //Rotate counter clockwise in array
        if dirIndex == 0 {
           (*robot).currentDirection = (*tableTop).availableDirection[3]
        }else {
              dirIndex -= 1
              (*robot).currentDirection = (*tableTop).availableDirection[dirIndex]        
        }
     
     }

}


// Turn right
func turnRight(robot *Robot, tableTop *TableTop){

     dirIndex := directionIndex((*robot).currentDirection, (*tableTop))
     if dirIndex != -1 {
        // rotate clockwise in array
        if dirIndex == 3 {
           (*robot).currentDirection = (*tableTop).availableDirection[0]
        }else {
              dirIndex += 1
              (*robot).currentDirection = (*tableTop).availableDirection[dirIndex]        
        }
     
     }

}


// move robot one up in direction its facing
func moveRobot(robot *Robot, tableTop *TableTop){
     //fmt.Println("Moving robot one unit forward", (*robot).currentDirection)

     if (*robot).currentDirection == (*tableTop).availableDirection[0] {  // facing north
        // Go North, incrment y by 1
        (*robot).setY((*robot).currentY + 1, tableTop)             
     }else if (*robot).currentDirection == (*tableTop).availableDirection[1] { // facing east
          (*robot).setX((*robot).currentX + 1, tableTop)     
     }else if (*robot).currentDirection == (*tableTop).availableDirection[2] { // facing south
     
           (*robot).setY((*robot).currentY - 1, tableTop)
           
     }else if (*robot).currentDirection == (*tableTop).availableDirection[3] { // facing west
     
          (*robot).setX((*robot).currentX - 1, tableTop)
          
     }

     //reportLocation()
}


/* Given a string array of place command, check right number of params
 * Ex: [PLACE X,Y,F]
*/
func parseXYAndF(xyfStr string) []string{

     // take [PLACE X,Y,F] and get second index to tokenize again by ','
     strTokensArray := strings.Split(xyfStr, ",")
     
     if len(strTokensArray) != 3 {
         fmt.Println("Bad input: ", strTokensArray)
         fmt.Println("Try: PLACE X,Y,F")
     }
     //fmt.Println("Parsed x,y and F to be: ", strTokensArray)
     return strTokensArray
}


//handle placing of robot on table
// return true if valid data set
func parsePlaceCommand(xyfStr string, tableTop *TableTop) (x int, y int, face string){

     // Specs don't specify what to do if invalid place number
     // Default to 0,0 for now
     x = 0
     y = 0
     face = ""
     //fmt.Println("Parse Place Command: ", xyfStr)

     strTokensArray := parseXYAndF(xyfStr)     

     targetX, err := strconv.Atoi(strTokensArray[0])
     if err != nil{
        return x,y,face  //if bad input return defaults
     }else{
        x = targetX        
     }//else


     // Parse Y param and check if valid then assign, else leave as 0
     targetY, err := strconv.Atoi(strTokensArray[1])
     if err != nil{
        return x,y,face
     }else{
           y = targetY
     }//else


     // valid direction check
     dirIndex := directionIndex(strTokensArray[2], *tableTop)
     if dirIndex == -1{
        return x,y,face
     }else{
        face = (*tableTop).availableDirection[dirIndex]
     }

     //If x, y or face is bad, return default value of robot
     
     //fmt.Println("X, Y, F = ", x, y, face)
     return x,y,face
}


/* Execute command on robot */
func executeRobotCommand(inputCommand string, robot *Robot, tt *TableTop){

     //split string by space
     tokenizedStringArray := strings.Split(inputCommand, " ")
         
     //if valid command (PLACE) then start robot
     if (tokenizedStringArray[0] == "PLACE" && len(tokenizedStringArray) == 2){
            x, y, f := parsePlaceCommand(tokenizedStringArray[1], tt)
            
            (*robot).setX(x, tt)
            (*robot).setY(y, tt)
            (*robot).setDirection(f, tt)

     }else if tokenizedStringArray[0] == "REPORT"{
               reportLocation(robot)
     }else if tokenizedStringArray[0] == "MOVE"{
               moveRobot(robot, tt)
     }else if tokenizedStringArray[0] == "LEFT"{
               turnLeft(robot, tt)
     }else if tokenizedStringArray[0] == "RIGHT"{
               turnRight(robot, tt)                     
     }else{
        // Ignore Bad Input
        //fmt.Println("Error: Invalid Command")
     }
         
}


/* Infinite for loop to listen for robot commands */
func listenForCommands(robot *Robot, tt *TableTop){

    for {
    
         reader := bufio.NewReader(os.Stdin)
         fmt.Print("===>>> Enter Command ==>  ")         
         inputCommand, _ := reader.ReadString('\n')  // read until enter was pressed

         inputCommand = strings.TrimSuffix(inputCommand, "\n")   //Tokenize into string array
     
         executeRobotCommand(inputCommand, robot, tt)

         
    }//infinite for looop waiting for PLACE Command         
}

     
func main() {

     fmt.Println("Welcome to Robotoy")
     fmt.Println("Type Ctrl-C to quit!")

     // Initialize Table top
     tt := TableTop{}
     tt.availableDirection = []string{"NORTH", "EAST", "SOUTH", "WEST"}
     tt.xMinBounds = 0
     tt.xMaxBounds = 4
     tt.yMinBounds = 0
     tt.yMaxBounds = 4

     robot := Robot{}  // Create robot shell 
     listenForCommands(&robot, &tt)
}