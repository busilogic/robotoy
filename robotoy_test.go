package main

import (
    "testing"
)

/* Hold array of commands and expected x,y,face */
type testCommands struct {
    robotCommand []string   // array of commands to robot
    positionX int   // where robot should be
    positionY int
    direction string
}

/* Array of testcommands to execute */
var tests = []testCommands{

    /*************************************************************************
     * Gibberish Data should be ignore, do not initialize robot
     ************************************************************************/
    { []string{"HIRE ME"}, 0, 0, "" },
    { []string{"I CAN CODE"}, 0, 0, "" },
    
    { []string{"I CAN COMMUNICATE"}, 0, 0, "" },
    { []string{"YOU CAN SEE ME GO!"}, 0, 0, "" },
    { []string{"PLACE 9,9,NINER"}, 0, 0, "" },
    { []string{"PLACE 0,9,NINER"}, 0, 0, "" },    
    { []string{"PLACE 8,19,TANGOBANGO"}, 0, 0, "" },
    
    //{ []string{"PLACE 8,19,TANGOBANGO"}, 1, 0, "" }, // Sanity Check by fail test with bad output

    /*************************************************************************
     * Ignore MOVE LEFT RIGHT BEFORE PLACE command called
     ************************************************************************/
    { []string{
      "MOVE",
      "LEFT",
      "RIGHT",
      "REPORT", // just outputs to command line but demo it doesn't affect robot
      },
      0, 0, "",
    },


    /*************************************************************************
     * Test placing robot in bad spot. Should ignore invalid params
     ************************************************************************/
    { []string{
      "PLACE 9,9,NORTH",
      },
      0, 0, "NORTH",
    },
    
    { []string{
      "PLACE 8,2,SOUTH",
      },
      0, 2, "SOUTH",  //only 2 and south are valid so they are okay robot ignore rest
    },


    /*************************************************************************
     * Make sure robot doesn't fall off the table at each corner
     # Visual layout of table top
     =====================================================================
     | (0,4)         | (1,4)         | (2,4)     |(3,4)      |(4,4)      |
     | (0,3)         | (1,3)         | (2,3)     |(3,3)      |(4,3)      |
     | (0,2)         | (1,2)         | (2,2)     |(3,2)      |(4,2)      |
     | (0,1)         | (1,1)         | (2,1)     |(3,1)      |(4,1)      |
     | (0,0)         | (1,0)         | (2,0)     |(3,0)      |(4,0)      |
     =====================================================================     
     ************************************************************************/

     // Test Robot doesn't fall off the bottom left corner
    { []string{
      "PLACE 0,0,SOUTH",
      "MOVE",
      },
      0, 0, "SOUTH",
    },

    { []string{
      "PLACE 0,0,SOUTH",
      "RIGHT",
      "MOVE",
      },
      0, 0, "WEST",
    },

     // Test Robot doesn't fall off the Top Left Corner
    { []string{
      "PLACE 0,4,NORTH",
      "MOVE",
      "MOVE",
      },
      0, 4, "NORTH",
    },

    { []string{
      "PLACE 0,4,WEST",
      "MOVE",
      },
      0, 4, "WEST",
    },

    // Test Robot doesn't fall off top right corner
    { []string{
      "PLACE 4,4,NORTH",
      "MOVE",
      },
      4, 4, "NORTH",
    },

    // Test Robot doesn't fall off top right corner
    { []string{
      "PLACE 4,4,EAST",
      "MOVE",
      },
      4, 4, "EAST",
    },

    // Place robot top right corner but face north and try to move
    { []string{
      "PLACE 4,4,EAST",
      "LEFT",
      "MOVE",
      },
      4, 4, "NORTH",
    },

    // Place robot bottom right corner 
    { []string{
      "PLACE 4,0,SOUTH",
      "MOVE",
      },
      4, 0, "SOUTH",
    },

    { []string{
      "PLACE 4,0,SOUTH",
      "LEFT",
      "MOVE",      
      },
      4, 0, "EAST",
    },
    
    // Make sure robot moves as expected    
    { []string{
      "PLACE 0,0,NORTH",
      "MOVE",
      },
      0, 1, "NORTH",
    },

    { []string{
      "PLACE 0,0,NORTH",
      "LEFT",
      },
      0, 0, "WEST",
    },

    { []string{
      "PLACE 1,2,EAST",
      "MOVE",
      "MOVE",
      "LEFT",
      "MOVE",            
      },
      3, 3, "NORTH",
    },

    { []string{
      "PLACE 0,3,EAST",
      "MOVE",
      "LEFT",
      "MOVE",
      "MOVE",                  
      },
      1, 4, "NORTH",
    },
    
}


// Test for gibberish input
func TestRoboToy(t *testing.T){

     t.Log("Starting tests for Robotoy") 

     // Initialize Table top
     tt := TableTop{}
     tt.availableDirection = []string{"NORTH", "EAST", "SOUTH", "WEST"}
     tt.xMinBounds = 0
     tt.xMaxBounds = 4
     tt.yMinBounds = 0
     tt.yMaxBounds = 4

     robot := Robot{}  // Create robot shell

     for _, testData := range tests {
     
         // Go through each array and execute command
         for _, command := range testData.robotCommand {
             executeRobotCommand(command, &robot, &tt)         
         }

         // Robot state should match expected output in test data, 
         if robot.currentX != testData.positionX || robot.currentY != testData.positionY || robot.currentDirection != testData.direction {         
            t.Error(
                "\n Entered Commands: \t", testData.robotCommand, "\n", 
                "expected Output:\t", testData.positionX, ", ", testData.positionY, " ", testData.direction, "\n",
                "Actual Output:\t", robot.currentX, ", ", robot.currentY, "  ", robot.currentDirection,
                )
         }
    }
    
     
}
